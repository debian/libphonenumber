libphonenumber (8.13.51+ds-4) unstable; urgency=medium

  * d/rules: Do not compress .xml files

 -- Matthias Geiger <werdahias@debian.org>  Mon, 09 Dec 2024 09:04:57 +0100

libphonenumber (8.13.51+ds-3) unstable; urgency=medium

  * Team upload.
  * Reinstate t64 Provides:

 -- Matthias Geiger <werdahias@debian.org>  Mon, 09 Dec 2024 00:08:07 +0100

libphonenumber (8.13.51+ds-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload

 -- Matthias Geiger <werdahias@debian.org>  Sun, 08 Dec 2024 23:30:13 +0100

libphonenumber (8.13.51+ds-1) unstable; urgency=medium

  * Team upload
  * New upstream version 8.13.51+ds
  * d/copyright: Update Files-Excluded list for new upstream
  * d/p: Refresh for new upstream, drop obsolete patches, cleanup series file
  * d/control: Depend on libgoogle-auto-value-java
  * libphonenumber-dev: Stop installing static library; include cmake files
  * d/rules: set hardening=+all
  * Updated lintian-overrides file for libphonenumber8-java
  * d/rules: Directly build with C++ 14; drop obsolete patch for this
  * Add d/u/metadata file
  * Set DEP-3 header for patches whose origin is clear
  * Ship resources/* in a libphonenumber-data package (Closes: #1089205)
  * d/gbp.conf: Add postimport section
  * Add d/salsa-ci.yml
  * Bump Standards-Version to 4.7.0; no changes needed
  * d/copyright: Update for new upstream

 -- Matthias Geiger <werdahias@debian.org>  Sun, 08 Dec 2024 23:27:38 +0100

libphonenumber (8.12.57+ds-4) unstable; urgency=medium

  * Team upload.
  * Use C++14 to resolve FTBFS with googletest 1.13.0 (Closes: #1041075)
    Thank you to Adrian Bunk for the patch

 -- tony mancill <tmancill@debian.org>  Wed, 19 Jul 2023 21:36:41 -0700

libphonenumber (8.12.57+ds-3) unstable; urgency=medium

  * Team upload.
  * Add patch to link geocoding-shared with phonenumber-shared
    (Closes: #1017496)
  * Build java lib in arch any builds (Closes: #1024768)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 24 Nov 2022 20:17:13 +0100

libphonenumber (8.12.57+ds-2) unstable; urgency=medium

  * Team upload.
  * Convert to debhelper
  * Expose protobuf ABI (Closes: #1024701)
  * Bump policy version (no changes)
  * Move package description to source package
  * Drop old Conflicts/Replaces

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 24 Nov 2022 12:03:20 +0100

libphonenumber (8.12.57+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.57+ds
    Metadata changes:
     - Updated phone metadata for region code(s):
       BJ, EH, GB, GF, GG, JE, MA, MW, SG, SN, SO, ZM
     - Updated geocoding data for country calling code(s): 229 (en)
     - Updated carrier data for country calling code(s):
       27 (en), 34 (en), 47 (en), 65 (en), 212 (en), 252 (en),
       260 (en), 594 (en), 974 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Fri, 14 Oct 2022 17:57:01 -0700

libphonenumber (8.12.56+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.56+ds
    Metadata changes:
     - Updated phone metadata for region code(s): HR, MK, PT, SG, TT
     - Updated short number metadata for region code(s): BZ
     - Updated carrier data for country calling code(s):
       31 (en), 65 (en), 385 (en), 389 (en)

 -- tony mancill <tmancill@debian.org>  Fri, 23 Sep 2022 08:43:20 -0700

libphonenumber (8.12.55+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.55+ds
    Metadata changes:
     - Updated phone metadata for region code(s):
       AU, CA, CC, CI, CO, CX, DE, HK, KW, LV, MV, PA, PL, TZ, US
     - Updated short number metadata for region code(s): CO, TZ
     - New geocoding data for country calling code(s): 1742 (en), 1753 (en)
     - Updated geocoding data for country calling code(s):
       57 (en), 225 (en), 960 (en)
     - New carrier data for country calling code(s): 371 (en)
     - Updated carrier data for country calling code(s):
       47 (en), 57 (en), 61 (en), 90 (en), 255 (en), 297 (en), 381 (en),
       420 (en), 972 (en), 974 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Fri, 09 Sep 2022 07:32:21 -0700

libphonenumber (8.12.54+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.54+ds
    Metadata changes:
     - Updated phone metadata for region code(s): BE, BF, GE, HN, MX, PT, US
     - Updated geocoding data for country calling code(s):
       52 (en), 61 (en), 351 (en)
     - Updated carrier data for country calling code(s):
       226 (en), 351 (en), 420 (en), 992 (en), 995 (en)
  * Remove unnecessary Java 5 patch

 -- tony mancill <tmancill@debian.org>  Thu, 25 Aug 2022 12:08:01 -0700

libphonenumber (8.12.53+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.52+ds
    Metadata changes:
     - Updated alternate formatting data for country calling code(s): 49
     - Updated phone metadata for region code(s):
       BW, DE, ET, HK, ML, MN, MQ, NP, PE, QA, SG
     - Updated geocoding data for country calling code(s): 61 (en)
     - Updated carrier data for country calling code(s):
       48 (en), 65 (en), 223 (en), 251 (en), 852 (en, zh), 976 (en), 977 (en)

  * New upstream version 8.12.53+ds
    Metadata changes:
     - Updated phone metadata for region code(s):
       AT, BE, CL, CN, GE, GF, GH, HK, JM, PG, RE, US
     - Updated short number metadata for region code(s): AT
     - New geocoding data for country calling code(s): 1943 (en)
     - Updated carrier data for country calling code(s):
       34 (en), 56 (en), 57 (en), 86 (en), 233 (en), 972 (en), 992 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Sun, 07 Aug 2022 16:29:23 -0700

libphonenumber (8.12.51+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.51+ds
    Metadata changes:
     - Updated phone metadata for region code(s):
       800, BJ, BR, CO, EH, FO, GE, GP, KE, KG, MA, MM, MN, MY, NZ, RE, SI,
       UG, VN
     - Updated geocoding data for country calling code(s): 57 (en), 61 (en)
     - Updated carrier data for country calling code(s):
       60 (en), 254 (en), 262 (en), 298 (en), 386 (en), 421 (en), 976 (en),
       995 (en), 996 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Wed, 06 Jul 2022 07:02:47 -0700

libphonenumber (8.12.50+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.50+ds
    Metadata changes:
     - Updated alternate formatting data for country calling codes: 380, 49
     - Updated phone metadata for region code(s):
       AR, CL, DE, EH, GB, HK, HR, IR, IT, MA, MH, TT, UA, US
     - New geocoding data for country calling code(s): 1826 (en)
     - Updated geocoding data for country calling code(s):
       34 (en, es), 54 (en), 61 (en)
     - Updated carrier data for country calling code(s):
       44 (en), 98 (en, fa), 212 (en), 380 (en, uk), 385 (en), 420 (en),
       852 (en, zh)
     - Updated / refreshed time zone meta data.
    New Metadata files:
     - The phone and short number metadata of all regions are available now
       in CSV format, at resources/metdata directory.
       (These are available in the Debian source package, but do not affect
       the binary packages generated from the sources.)

 -- tony mancill <tmancill@debian.org>  Sat, 25 Jun 2022 07:37:10 -0700

libphonenumber (8.12.49+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.49+ds
    Metadata changes:
     - Updated phone metadata for region code(s):
       CA, CM, GB, IL, JM, JP, MA, MV, PG, US
     - Updated geocoding data for country calling code(s):
       33 (en), 44 (en), 212 (en, fr), 1310 (en)
     - Updated carrier data for country calling code(s):
       237 (en), 675 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Fri, 03 Jun 2022 22:08:10 -0700

libphonenumber (8.12.48+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.48
    release notes: https://github.com/google/libphonenumber/pull/2767
  * Update debian/copyright to exclude JAR files from upstream sources
    and update debian/watch to repack with +ds version suffix.

 -- tony mancill <tmancill@debian.org>  Sat, 07 May 2022 21:15:08 -0700

libphonenumber (8.12.47-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.47
  * Refresh patches for new upstream version
  * Add build-dep on libmockito-java

 -- tony mancill <tmancill@debian.org>  Fri, 22 Apr 2022 07:20:10 -0700

libphonenumber (8.12.46-2) unstable; urgency=medium

  * Team upload.
  * Add patch to resolve FTBFS for reverse build-deps (Closes: #1008817)
    - Thank you to Adrian Bunk and Christoph Anton Mitterer.

 -- tony mancill <tmancill@debian.org>  Sat, 02 Apr 2022 10:43:51 -0700

libphonenumber (8.12.46-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.46
    Metadata changes:
     - Updated phone metadata for region code(s): BJ, JM, PW, SA
     - Updated short number metadata for region code(s): HU
     - Updated carrier data for country calling code(s):
       36 (en), 51 (en), 61 (en), 90 (en), 229 (en), 254 (en), 680 (en),
       966 (en)
  * Changelog from upstream version 8.12.45 (not released for Debian)
    Metadata changes:
     - Updated phone metadata for region code(s): BF, EE, JM, RE, SE, US
     - New geocoding data for country calling code(s): 1464 (en)
     - Updated carrier data for country calling code(s):
       46 (en), 55 (en), 226 (en), 262 (en), 353 (en), 372 (en), 373 (en),
       1345 (en)
     - Updated / refreshed time zone meta data.

 -- tony mancill <tmancill@debian.org>  Fri, 01 Apr 2022 09:53:47 -0700

libphonenumber (8.12.44-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.44
    Metadata changes:
     - Updated phone metadata for region code(s):
       AG, AI, AS, BB, BM, BS, CA, CV, DM, DO, GD, GU, JM, KN, KY, LC, MP, MS, PR,
       SC, SX, TC, TT, US, VC, VG, VI
     - Updated short number metadata for region code(s): BE, PT, SC, SE, US
     - Updated geocoding data for country calling code(s):
       61 (en), 238 (en), 1345 (en)
     - Updated carrier data for country calling code(s): 238 (en), 248 (en)

 -- tony mancill <tmancill@debian.org>  Thu, 24 Feb 2022 06:56:52 -0800

libphonenumber (8.12.43-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.43
    Metadata changes:
    - Updated phone metadata for region code(s):
        BJ, CL, GA, IS, KR, KW, LI, PA, SG, SL
    - Updated short number metadata for region code(s): MS
    - Updated geocoding data for country calling code(s): 56 (en, es)
    - Updated carrier data for country calling code(s):
        32 (en), 229 (en), 354 (en), 502 (en), 507 (en)

 -- tony mancill <tmancill@debian.org>  Wed, 16 Feb 2022 20:57:19 -0800

libphonenumber (8.12.42-2) unstable; urgency=medium

  * Team upload.
  * libphonenumber-dev: add dependency on libabsl-dev (Closes: #1005157)

 -- tony mancill <tmancill@debian.org>  Tue, 08 Feb 2022 07:14:02 -0800

libphonenumber (8.12.42-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.42
  * Refresh patches for new upstream version
  * Drop unnecessary JDK5 5 compilation patch

 -- tony mancill <tmancill@debian.org>  Mon, 31 Jan 2022 20:03:33 -0800

libphonenumber (8.12.41-2) unstable; urgency=medium

  * Team upload.
  * Eliminate build parallelism to try to work around build failures

 -- tony mancill <tmancill@debian.org>  Fri, 28 Jan 2022 14:35:18 -0800

libphonenumber (8.12.41-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.41 (Closes: #1003972)
  * Add build-dep on libabsl-dev
  * Add patch to build against Debian packaging of abseil
  * Replace obsolete libservlet3.1-java with libservlet-api-java
    and remove maven.rules entry that remapped javax.servlet
  * Drop needless build-dep on libservlet3.1-java-doc

 -- tony mancill <tmancill@debian.org>  Thu, 27 Jan 2022 07:48:33 -0800

libphonenumber (8.12.16-4) unstable; urgency=medium

  * Team upload.
  * Update Vcs-URLs to salsa.d.o
  * Depend on protobuf-api-* (Closes: #980423)
  * Add MA hint
  * simplify d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 23 Jan 2021 21:16:40 +0100

libphonenumber (8.12.16-3) unstable; urgency=medium

  * Team upload.
  * libphonenumber-dev: Depend on libboost-thread-dev. I don't think that
    is the correct thing to do, and packages like e-d-s should declare
    that build dependency explicitly. See #978172.
  * Remove oldish version requirements in (build-)dependencies.

 -- Matthias Klose <doko@debian.org>  Wed, 20 Jan 2021 13:29:07 +0100

libphonenumber (8.12.16-2) experimental; urgency=medium

  * Team upload.
  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Tue, 19 Jan 2021 18:12:42 +0100

libphonenumber (7.1.0-7) unstable; urgency=medium

  * Team upload.
  * Don't build using boost, not required on recent Linux versions.
  * Make the build verbose.
  * Properly pass optimization and hardening flags to the cmake build.
  * Fix the clean target.
  * Bump debhelper and standards version.

 -- Matthias Klose <doko@debian.org>  Wed, 16 Dec 2020 14:49:19 +0100

libphonenumber (7.1.0-6) unstable; urgency=medium

  * Team upload.
  * Update watch and Vcs package attributes, thanks to Neil Mayhew for the
    patch. Closes: #956863
  * Update the Uploaders List. Closes: #953464
  * Add gbp.conf specifing the Debian default branch.

 -- Tobias Frost <tobi@debian.org>  Tue, 29 Sep 2020 19:08:25 +0200

libphonenumber (7.1.0-5) unstable; urgency=medium

  * Team upload.
  * Revert "Set environment to GCC-6." because this is the default now.
  * Add readdir_r-is-deprecated.patch and fix FTBFS with glibc 2.24.
    Thanks to Graham Inggs for the report. (Closes: #836768)
  * Add libboost-filesystem-dev to Build-Depends.

 -- Markus Koschany <apo@debian.org>  Tue, 13 Sep 2016 23:13:45 +0200

libphonenumber (7.1.0-4) unstable; urgency=medium

  * Team upload.
  * Vcs-Git: Use https.
  * Add gcc-6-ftbfs.patch and fix FTBFS with GCC-6. (Closes: #811613)
    Thanks to Martin Michlmayr for the report.
  * Fix Lintian warning syntax-error-in-dep5-copyright.
  * Add 0010-reproducible-build.patch and make the build reproducible.
    Thanks to Reiner Herrmann for the report and patch. (Closes: #828748)
  * Fix Lintian warning dep5-copyright-license-name-not-unique.

 -- Markus Koschany <apo@debian.org>  Sat, 16 Jul 2016 23:04:13 +0200

libphonenumber (7.1.0-3) unstable; urgency=medium

  * Team upload.
  * Transition to the Servlet API 3.1 (Closes: #801036)
  * Standards-Version updated to 3.9.8 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 21 Jun 2016 09:54:33 +0200

libphonenumber (7.1.0-2) unstable; urgency=medium

  * Move libphonenumber7-dev.install to libphonenumber-dev.install so the -dev
    package actually contains files.
  * Have libphoneumber7-java Conflict & Replace libphonenumber-java.

 -- Iain Lane <laney@debian.org>  Tue, 10 Nov 2015 09:57:19 +0000

libphonenumber (7.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Conflict with libphonenumber6-java.  (Closes: #800921)

 -- Daniel Pocock <daniel@pocock.pro>  Fri, 09 Oct 2015 15:00:45 +0200

libphonenumber (7.0.11-2) unstable; urgency=medium

  * Split libgeocoding out into a separate package.
  * Make library packages Multi-Arch: same
  * Rename -dev package to not include the version - this means that
    our reverse build-deps don't need to have source changes whenever we bump
    the ABI version.
  * debian/patches/0001-Boost-build-fix.patch: Make sure to install all the
    headers.
  * debian/patches/0002-C-symbols-map.patch: Control our exported symbols to
    not leak those of libraries we depend on.

 -- Iain Lane <laney@debian.org>  Fri, 25 Sep 2015 13:20:24 +0100

libphonenumber (7.0.11-1) unstable; urgency=medium

  * New upstream release.
  * VCS rebased using clone of new upstream Git repository.
  * Builds with GCC-5.  (Closes: #797835)

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 22 Sep 2015 11:38:16 +0000

libphonenumber (6.3~svn698-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against libboost in unstable (that is >= 1.58).
    - (Closes: #794900, #794658)
  * Add patch to remove the deprecated forkMode parameter in the
    maven-surefire-plugin configuration.  This was causing tests
    to fail and made the package FTBFS.
  * Test compilations with GCC-5 from experimental also indicate
    that this package now successfully builds without source changes
    (surely due to the updated libboost).  (Closes: #777967)

 -- tony mancill <tmancill@debian.org>  Sun, 09 Aug 2015 13:09:47 -0700

libphonenumber (6.3~svn698-3) unstable; urgency=medium

  * More patches for JDK 1.5 build.

 -- Daniel Pocock <daniel@pocock.pro>  Wed, 03 Sep 2014 07:12:50 +0200

libphonenumber (6.3~svn698-2) unstable; urgency=medium

  * Add patches for JDK 1.5 build.

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 02 Sep 2014 20:47:56 +0200

libphonenumber (6.3~svn698-1) unstable; urgency=medium

  * Latest upstream snapshot.

 -- Daniel Pocock <daniel@pocock.pro>  Thu, 28 Aug 2014 10:59:31 +0200

libphonenumber (6.3~svn681-5) unstable; urgency=medium

  * Ensure JARs are installed to the correct locations.

 -- Daniel Pocock <daniel@pocock.pro>  Wed, 20 Aug 2014 10:28:18 +0200

libphonenumber (6.3~svn681-4) unstable; urgency=medium

  * Fix for issue with large unsigned long.

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 19 Aug 2014 15:32:10 +0200

libphonenumber (6.3~svn681-3) unstable; urgency=medium

  * Remove Build-Depends-Indep as Java needed for C++ build.

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 19 Aug 2014 14:41:08 +0200

libphonenumber (6.3~svn681-2) unstable; urgency=medium

  * Remove patch for Maven plugin versions.

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 19 Aug 2014 14:00:20 +0200

libphonenumber (6.3~svn681-1) unstable; urgency=medium

  * Latest upstream code.

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 19 Aug 2014 10:15:18 +0200

libphonenumber (6.3~svn680-2) unstable; urgency=medium

  * Initial packaging (Closes: #741258)

 -- Daniel Pocock <daniel@pocock.pro>  Sun, 15 Jun 2014 13:11:35 +0200
